"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiConfig = void 0;
exports.apiConfig = {
    baseURL: '',
    method: 'GET',
    url: '',
    // 配置请求头信息
    headers: {
        'content-type': 'application/json;charset=UTF-8',
    },
    pending: {},
    showLoading: function () { },
    hideLoading: function () { },
};
