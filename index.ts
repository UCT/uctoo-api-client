import api from './lib/request/index';
const Router = require('koa-router'); // koa 路由中间件

const Koa = require('koa');

const app = new Koa();
const router = new Router(); // 实例化路由
router.get('/api', async (ctx: any, next: any) => {
	ctx.response.body = '<h5>Index</h5>';
	const request = api.create({
		baseURL: 'http://42.240.131.35:9000/api',
		headers: {
			Authentication:
				'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MGQwMjM1ZDRiZGU5ZTk5NDUxZDgzYjkiLCJpYXQiOjE2MjQyNTQyNzIsImV4cCI6MTYyNDM0MDY3Mn0.0glc-fj4-K_TWpPQvKzgcuVjNI-aMYOk6Fvx2K8Yug4',
		},
		showLoading: () => {
			console.log(11);
		},
		hideLoading: () => {
			console.log(22);
		},
		pending: {
			'/users/login': () => {
				console.log(222);
			},
		},
	});

	request
		.post('/users/login', {
			user: {
				email: 'jake2@jake.jake',
				password: 'jakejake',
			},
		})
		.then(res => {
			console.log(res);
		});
});
app.use(router.routes());
//设置监听端口
app.listen(3000, () => {
	console.log('服务器开启 127.0.0.1:3000');
});
