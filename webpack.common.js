const path = require('path');
let config = {};

function generateConfig(name) {
	const config = {
		entry: './index.js',
		module: {
			rules: [
				{
					test: /\.ts?$/,
					use: 'ts-loader',
					exclude: /node_modules/,
				},
			],
		},
		resolve: {
			extensions: ['.tsx', '.ts', '.js'],
		},
		output: {
			path: __dirname + '/dist/',
			filename: name + '.js',
			library: 'request',
			libraryTarget: 'umd',
		},
	};

	return config;
}

['axios'].forEach(function (key) {
	config = generateConfig(key);
});

module.exports = config;
