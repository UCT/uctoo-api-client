const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');
const config = {};

function generateConfig(name) {
	const compress = name.indexOf('min') > -1;
	const config = {
		entry: './index.js',
		output: {
			path: __dirname + '/dist/',
			filename: name + '.js',
			sourceMapFilename: name + '.map',
			library: 'axios',
			libraryTarget: 'umd',
		},
		node: {
			process: false,
		},
		devtool: 'source-map',
		mode: compress ? 'production' : 'development',
	};
	return config;
}

['axios', 'axios.min'].forEach(function (key) {
	config[key] = generateConfig(key);
});

module.exports = config;
