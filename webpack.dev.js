const commonConfig = require("./webpack.common");
const { merge } = require("webpack-merge");

const path = require("path");

module.exports = merge(commonConfig, {
  mode: "development",
  devtool: "eval-cheap-module-source-map",
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    port: 8000,
    open: true,
    hot: true,
  },
});
